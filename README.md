# Nginx Proxy 1.0

This is my Nginx umbrella container, which automatically generates the nginx configuration file necessary for all running docker services.

With this I get an overview on what services exist and which of them are up & running, and available under which domains.

Also it updates my `/etc/hosts` if a new service is available and I made a volume mount. - This is neccessary for local / development containers, so I can connect to them via their domain name without having to care about anything but a simple configuration in the container as I will explain below.

Ensure that port 80 / 443 is available on your host system since this is planned to be the one and only nginx running.

If you have more generic questions please have a look [here](https://cms.manhart.space/dockerisierung)

## Getting Started

Start the container with the command:

    # sudo docker-compose up -d

Your umbrella nginx is available on http://ls.local/

### Adding services

To add a new service, you have to do the following things:

1. Declare the network of the nginx proxy

		networks:
			proxy:
				external:
					name: proxy

2. Put it in the network of the nginx proxy

		networks:
			- proxy

3. Set the virtual host by adding an environment variable called VIRTUAL_HOST

		expose:
			- "80"

4. Ensure that the port is exposed (usually this is done by the docker image nowadays)

So a full example service would look like this:

	services:
	web:
		image: nginx:alpine
		expose:
			- "80"
		environment:
			VIRTUAL_HOST: php.local
		networks:
			- proxy
			- internal
		restart: unless-stopped
	php:
		image: php:7-fpm-alpine
		networks:
		- internal
		restart: unless-stopped
	networks:
		proxy:
			external:
				name: proxy
		internal-networ
			driver: bridge

### Automatically adding the services into the hosts file

If you mount your `/etc/hosts` file into the container it will automatically add the services into there, so you can access them via their hostnames.

This is important / interesting for local development.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down
	
to tear down the service.

Then pull the latest version with

	# docker-compose pull

Lastly start the service with

	# docker-compose up -d

### Environment variables

__You can configure following environment variables__

inside the `docker-compose.yml` file:

`NGINX_PROXY_FILE` - the path to the nginx proxy config file where all services are included (default points to `INTERNAL_NGINX_CONF_DIR/default.conf`)
`HTML_TEMPLATE_FILE` - the path to the handlebars html template file on the host system - you can replace the default template with that

inside `.env` file:

`VIRTUAL_HOST` - the name of the overview app in the jwilder/nginx-proxy (default `http://overview.local/`)
`LOCAL_OVERVIEW_APP_PATH` - used for developing the overview app, should point to the app dir on the host system (default deactivated)
`LOCAL_HOSTS_FILE_PATH` - the path of the hosts file on the host system (default `/etc/hosts`)
`INTERNAL_NGINX_CONF_DIR` - the path inside the container to the nginx config dir (default `/etc/nginx/conf.d`)

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/php-dev/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__1.0__

* Added the service
* Added the overview app

### Open issues

* Describe how to add https support / automated letsencrypt certificates

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [JWilder Nginx Proxy Image](https://hub.docker.com/r/jwilder/nginx-proxy)
