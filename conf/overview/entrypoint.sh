#!/bin/sh

APP_PATH=/home/node/app
PACKAGE_JSON=${APP_PATH}/package.json
DEFAULT_OVERVIEW_APP_REPO=https://manuelmanhart@bitbucket.org/mmprivat/nginx-proxy-overview-app.git

if [ ! -d "$APP_PATH" ]; then
	echo "Creating app dir '$APP_PATH'"
	mkdir -p $APP_PATH
fi

cd $APP_PATH

if [ ! -f "$PACKAGE_JSON" ]; then
	if [ -z "$OVERVIEW_APP_REPO" ]; then
		OVERVIEW_APP_REPO=$DEFAULT_OVERVIEW_APP_REPO
	fi
	apk add --update-cache git
	echo "Downloading app from '$OVERVIEW_APP_REPO'"
	#wget https://bitbucket.org/mmprivat/nginx-proxy-overview-app/get/a23604698dcb.zip
	git clone $OVERVIEW_APP_REPO $APP_PATH
fi

if [ ! -d "$APP_PATH/node_modules" ]; then
	echo "Running 'npm install'"
	npm install
fi

if [ -n "$OVERVIEW_APP_DEBUG" ]; then
	echo "Starting overview app with 'npm run dev'"
	npm run dev
else 
	echo "Starting overview app with 'npm start'"
	npm start
fi
